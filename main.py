import tkinter as tk
from PIL import Image, ImageTk
import random
import os
from tkinter import messagebox
from components import AttendencePage
from components import StudentOperationDialog
from components import ClassOperationDialog
from components import ImportStudentsDialog


class SchoolAttendanceSystem:
    def __init__(self, master):
        self.master = master
        self.master.title("学校点名系统")
        self.master.geometry("800x640")

        self.menu = tk.Menu(master)
        self.master.config(menu=self.menu)
        self.student_menu = tk.Menu(self.menu, tearoff=0)

        self.menu.add_cascade(label="班级管理", menu=self.student_menu)
        self.student_menu.add_command(label="添加班级", command=self.add_class)
        self.student_menu.add_command(label="删除班级", command=self.delete_class)
        self.student_menu.add_command(label="重新设置当前班级", command=self.set_class)

        self.student_menu = tk.Menu(self.menu, tearoff=0)
        self.menu.add_cascade(label="学生管理", menu=self.student_menu)
        self.student_menu.add_command(label="添加学生", command=self.add_student)
        self.student_menu.add_command(label="删除学生", command=self.delete_student)
        self.student_menu.add_command(label="批量导入学生", command=self.import_students)

        self.attendance_menu = tk.Menu(self.menu, tearoff=0)
        self.menu.add_cascade(label="缺勤管理", menu=self.attendance_menu)
        self.attendance_menu.add_command(label="管理缺勤情况", command=self.manage_attendance)
        self.attendance_menu.add_command(label="缺勤清零", command=self.clear_attendance)

        # 使用Pillow打开图片并转换为Tkinter支持的格式
        self.background_image = Image.open("./image/bg.jpg")
        self.background_photo = ImageTk.PhotoImage(self.background_image)

        # 创建画布并显示图片
        self.canvas = tk.Canvas(master, width=800, height=640)
        self.canvas.pack(fill=tk.BOTH, expand=True)
        self.canvas.create_image(0, 0, image=self.background_photo, anchor=tk.NW)

        # 从文件中读取并翻译班级信息
        self.class_info = self.read_class_info_from_file("./data/current_class.txt")
        class_details = self.translate_class_info(self.class_info)
        self.grade = class_details[0]
        self.classNo = class_details[1]
        translated_info = f'当前班级: {self.grade}年级{self.classNo}班'
        classfile_name = f"./data/class/{self.grade}-{self.classNo}.txt"
        self.students = self.read_file_lines(classfile_name)
        self.classes = self.list_files_in_directory("./data/class/")

        # 创建标签来显示翻译后的班级信息
        self.class_label = self.canvas.create_text(20, 20, text=translated_info, font=("Arial", 12, "bold"),
                                                   anchor=tk.NW, fill="white")

        # 在画布中央添加按钮
        self.button_image = Image.open("./image/button_bg.jpg")
        self.button_image = self.button_image.resize((150, 75), Image.Resampling.LANCZOS)
        self.button_photo = ImageTk.PhotoImage(self.button_image)
        self.button = tk.Button(self.canvas, image=self.button_photo, command=self.random_attendance,
                                font=("Arial", 14, "bold"), fg="white", text="随机点名", compound="center",
                                relief=tk.RAISED, padx=0.5, pady=0.5, borderwidth=0.1)
        self.canvas.create_window(400, 380, window=self.button)

        # 在画布中创建一个用于显示点名结果的文本对象
        self.result_text_1 = None
        self.result_text_2 = None

    @staticmethod
    def list_files_in_directory(folder_path):
        # 获取文件夹中的所有文件
        files = os.listdir(folder_path)
        # 输出文件列表
        files_list = []
        for file in files:
            # 获取文件的完整路径并打印
            file_name = os.path.basename(file)
            files_list.append(file_name)
        return files_list

    @staticmethod
    def read_class_info_from_file(filename):
        with open(filename, "r", encoding="utf-8") as file:
            return file.read()

    @staticmethod
    def translate_class_info(class_info):
        parts = class_info.split(';')
        grade = parts[0].split(':')[1]
        class_num = parts[1].split(':')[1]
        result = [grade, class_num]
        return result

    @staticmethod
    def read_file_lines(filename):
        lines = []
        with open(filename, "r", encoding="utf-8") as file:
            for line in file:
                lines.append(line.strip())
        return lines

    def add_class(self):
        def confirm(grade_entry, class_entry, error_label, class_dialog):
            grade = grade_entry.get()
            class_num = class_entry.get()
            # 保存班级信息等
            new_file_name = f'{grade}-{class_num}.txt'
            if self.classes.count(new_file_name) == 1:
                error_label.config(text="该班级已经存在！", fg="red", state="normal")
                return
            with open(f'./data/class/{new_file_name}', 'w') as file:
                file.write('')
            with open(f'./data/attendence/{new_file_name}', 'w') as file:
                file.write('')
            class_dialog.destroy()
            self.classes = self.list_files_in_directory("./data/class/")
            messagebox.showinfo("成功", "成功添加班级！")

        ClassOperationDialog(self.master, "添加班级", confirm)

    def delete_class(self):
        def confirm(grade_entry, class_entry, error_label, class_dialog):
            grade = grade_entry.get()
            class_num = class_entry.get()
            self.class_info = self.read_class_info_from_file("./data/current_class.txt")
            class_details = self.translate_class_info(self.class_info)
            self.grade = class_details[0]
            self.classNo = class_details[1]
            if grade == self.grade and self.classNo == class_num:
                error_label.config(text="改班级当前正在使用，不可以删除！", fg="red", state="normal")
                return
            # 保存班级信息等
            file_name = f'{grade}-{class_num}.txt'
            file_path = f"./data/class/{file_name}"
            if self.classes.count(file_name) == 0:
                error_label.config(text="该班级不存在！", fg="red", state="normal")
                return
            os.remove(file_path)
            attend_file_path = f"./data/attendence/{file_name}"
            os.remove(attend_file_path)
            class_dialog.destroy()
            self.classes = self.list_files_in_directory("./data/class/")
            messagebox.showinfo("成功", "成功删除班级！")

        ClassOperationDialog(self.master, "删除班级", confirm)

    def set_class(self):
        def confirm(grade_entry, class_entry, error_label, class_dialog):
            self.grade = grade_entry.get()
            self.class_num = class_entry.get()
            file_name = f'{self.grade}-{self.class_num}.txt'
            file_path = f"./data/class/{file_name}"
            if self.classes.count(file_name) == 0:
                error_label.config(text="该班级不存在！", fg="red", state="normal")
                return
            self.students = self.read_file_lines(file_path)
            translated_info = f'当前班级: {self.grade}年级{self.class_num}班'
            self.canvas.delete(self.class_label)
            # 创建新的班级信息显示
            self.class_label = self.canvas.create_text(20, 20, text=translated_info, font=("Arial", 12, "bold"),
                                                       anchor=tk.NW,
                                                       fill="white")
            class_dialog.destroy()
            with open("./data/current_class.txt", "w", encoding="utf-8") as file:
                file.write(f'grade:{self.grade};class:{self.class_num}')
            messagebox.showinfo("成功", "当前班级更改成功！")

        ClassOperationDialog(self.master, "选择班级", confirm)
        self.class_info = self.read_class_info_from_file("./data/current_class.txt")
        class_details = self.translate_class_info(self.class_info)
        self.grade = class_details[0]
        self.classNo = class_details[1]

    def add_student(self):
        # 实现添加学生功能
        def confirm(grade_entry, class_entry, error_label, class_dialog, student_entry):
            grade = grade_entry.get()
            class_num = class_entry.get()
            student_name = student_entry.get()
            file_name = f'{grade}-{class_num}.txt'
            file_path = f"./data/class/{file_name}"
            attendence_file_path = f"./data/attendence/{file_name}"
            students = self.read_file_lines(file_path)
            if students.count(student_name) == 1:
                error_label.config(text=f"该学生已存在{grade}年{class_num}班！", fg="red", state="normal")
                return
            with open(file_path, "a", encoding="utf-8") as file:
                file.write(f'{student_name}\n')
            with open(attendence_file_path, "a", encoding="utf-8") as file:
                file.write(f'{student_name}:0\n')
            class_dialog.destroy()
            self.classes = self.list_files_in_directory("./data/class/")
            messagebox.showinfo("成功", "成功添加学生！")

        StudentOperationDialog(self.master, "添加学生", confirm)

    def manage_attendance(self):
        # 创建缺勤查看窗口
        self.attendance_window = tk.Tk()
        self.attendance_window.title("管理缺勤情况")
        self.attendance_window.geometry("400x300")

        # 计算窗口的中心位置
        screen_width = self.attendance_window.winfo_screenwidth()
        screen_height = self.attendance_window.winfo_screenheight()
        x = (screen_width - 400) // 2
        y = (screen_height - 300) // 2
        self.attendance_window.geometry(f"400x300+{x}+{y}")
        self.class_info = self.read_class_info_from_file("./data/current_class.txt")
        class_details = self.translate_class_info(self.class_info)
        self.grade = class_details[0]
        self.classNo = class_details[1]
        attendfile_name = f"./data/attendence/{self.grade}-{self.classNo}.txt"
        with open(attendfile_name, "r", encoding="utf-8") as file:
            attendance_data = [line.strip() for line in file.readlines()]

        def update_data(name, value):
            with open(attendfile_name, "r", encoding="utf-8") as file:
                attendance_data = file.readlines()
            # 查找并更新名字对应的行
            for i, line in enumerate(attendance_data):
                if name in line:
                    parts = line.split(":")
                    new_line = f"{parts[0]}: {value}\n"
                    attendance_data[i] = new_line
                    break

            # 写回文件
            with open(attendfile_name, "w", encoding="utf-8") as file:
                file.writelines(attendance_data)

        self.page = AttendencePage(self.attendance_window, attendance_data, update_data)
        self.page.pack(fill=tk.BOTH, expand=True)

        button_frame = tk.Frame(self.attendance_window)
        button_frame.pack(side=tk.BOTTOM, padx=5, pady=5)

        close_button = tk.Button(button_frame, text="关闭", command=self.attendance_window.destroy)
        close_button.pack(side=tk.RIGHT, padx=5, pady=5)
        # 上一页按钮
        prev_button = tk.Button(button_frame, text="上一页", command=self.page.prev_page)
        prev_button.pack(side=tk.RIGHT, padx=5, pady=5)

        # 下一页按钮
        next_button = tk.Button(button_frame, text="下一页", command=self.page.next_page)
        next_button.pack(side=tk.RIGHT, padx=5, pady=5)

        self.attendance_window.mainloop()

    def delete_student(self):
        # 实现删除学生功能
        def confirm(grade_entry, class_entry, error_label, class_dialog, student_entry):
            grade = grade_entry.get()
            class_num = class_entry.get()
            student_name = student_entry.get()
            file_name = f'{grade}-{class_num}.txt'
            file_path = f"./data/class/{file_name}"
            students = self.read_file_lines(file_path)
            if students.count(student_name) == 0:
                error_label.config(text=f"该学生不存在{grade}年{class_num}班！", fg="red", state="normal")
                return
            with open(file_path, "r", encoding="utf-8") as file:
                lines = file.readlines()
            lines = [line for line in lines if line.strip() != student_name]
            with open(file_path, "w", encoding="utf-8") as file:
                file.writelines(lines)

            attendence_file_path = f"./data/attendence/{file_name}"
            with open(attendence_file_path, "r", encoding="utf-8") as file:
                lines = file.readlines()
            lines = [line for line in lines if line.strip().count(student_name) == 0]
            with open(attendence_file_path, "w", encoding="utf-8") as file:
                file.writelines(lines)
            class_dialog.destroy()
            self.classes = self.list_files_in_directory("./data/class/")
            messagebox.showinfo("成功", "成功删除学生！")

        StudentOperationDialog(self.master, "删除学生", confirm)

    def import_students(self):
        def import_callback(grade, class_num, file_path):
            with open(file_path, "r", encoding="utf-8") as file:
                students_list = file.readlines()
            file_path = f"./data/class/{grade}-{class_num}.txt"
            attendence_file_path = f"./data/attendence/{grade}-{class_num}.txt"
            confirm = messagebox.askyesno("确认覆盖数据", "导入新数据将覆盖现有数据，是否继续？")
            if confirm:
                with open(file_path, "w", encoding="utf-8") as file, open(attendence_file_path, "w", encoding="utf-8") as file1:
                    file.writelines(students_list)
                    data = [a.strip() + ':0\n' for a in students_list]
                    file1.writelines(data)
                messagebox.showinfo("成功", "成功导入！")
        dialog = ImportStudentsDialog(self.master, import_callback)

    def clear_attendance(self):
        print()
        confirm = messagebox.askyesno("确认清零", "当前班级的缺席数据将被清零，是否继续？")
        if confirm:
            classfile_name = f"./data/class/{self.grade}-{self.classNo}.txt"
            attendencefile_name = f"./data/attendence/{self.grade}-{self.classNo}.txt"
            with open(classfile_name, "r", encoding="utf-8") as class_file, open(attendencefile_name, "w",
                                                                                 encoding="utf-8") as attendance_file:
                for line in class_file:
                    line = line.strip() + ":0\n"  # 在每行末尾添加 ":0"，表示初始缺勤次数还原为0
                    attendance_file.write(line)
            messagebox.showinfo("成功", "当前班级的缺勤数据已成功清零！")

    def random_attendance(self):
        # 清除画布上的所有名字
        self.clear_previous_names()

        # 随机点名功能
        self.animate_names(self.students)

    def clear_previous_names(self):
        # 清除画布上的所有名字
        for item in self.canvas.find_withtag("name"):
            self.canvas.delete(item)
        # 清除之前选中的名字
        if self.result_text_1 is not None:
            self.canvas.delete(self.result_text_1)
            self.result_text_1 = None
        if self.result_text_2 is not None:
            self.canvas.delete(self.result_text_2)
            self.result_text_2 = None

    def animate_names(self, names):
        # 随机生成名字的初始位置
        initial_positions = [(random.randint(80, 720), random.randint(80, 560)) for _ in names]

        # 在画布上显示名字，并获取名字对象的id
        name_objs = [self.canvas.create_text(pos[0], pos[1], text=name, font=("Comic Sans MS", 20), fill="white",
                                             anchor=tk.CENTER, tags="name") for name, pos in
                     zip(names, initial_positions)]

        # 更新画布，确保名字显示出来
        self.master.update()

        # 控制飞动次数
        for _ in range(50):
            # 随机更新名字的位置
            for name_obj in name_objs:
                dx = random.randint(-30, 30)
                dy = random.randint(-30, 30)
                self.canvas.move(name_obj, dx, dy)
            # 更新画布，产生动画效果
            self.master.update()
            self.master.after(50)  # 延时，控制动画速度

        # 随机选中两个名字
        selected_names = random.sample(names, 2)
        name1, name2 = selected_names

        # 清除飞舞的名字
        for name_obj in name_objs:
            self.canvas.delete(name_obj)

        # 在屏幕中央显示选中的两个名字
        self.result_text_1 = self.canvas.create_text(330, 280, text=name1, font=("Comic Sans MS", 30), fill="white",
                                                     anchor=tk.CENTER)
        self.result_text_2 = self.canvas.create_text(470, 280, text=name2, font=("Comic Sans MS", 30), fill="white",
                                                     anchor=tk.CENTER)

    def center_window(self):
        # 获取屏幕宽度和高度
        screen_width = self.master.winfo_screenwidth()
        screen_height = self.master.winfo_screenheight()

        # 计算窗口宽度和高度
        window_width = 800 # 这里假设窗口宽度为400
        window_height = 640  # 这里假设窗口高度为300

        # 计算窗口在屏幕上的左上角位置
        x = (screen_width - window_width) // 2
        y = (screen_height - window_height) // 2

        # 将窗口移动到计算出的位置
        self.master.geometry(f"{window_width}x{window_height}+{x}+{y}")


def main():
    root = tk.Tk()
    app = SchoolAttendanceSystem(root)
    app.center_window()
    root.mainloop()


if __name__ == "__main__":
    main()
