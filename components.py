import tkinter as tk
from tkinter import filedialog
import math


class AttendencePage(tk.Frame):
    def __init__(self, parent, data, update_data_callback, rows_per_page=10):
        tk.Frame.__init__(self, parent)
        self.data = [s.split(":") for s in data]
        for item in self.data:
            item[1] = item[1].strip()  # 去除换行符
        self.rows_per_page = rows_per_page
        self.current_page = 1

        self.page_frame = tk.Frame(self, bg="white")  # 将页面框架背景颜色更改为白色
        self.page_frame.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        self.update_data_callback = update_data_callback
        self.buttons = []
        self.update_page()

    def update_page(self):
        start_index = (self.current_page - 1) * self.rows_per_page
        end_index = start_index + self.rows_per_page
        page_data = self.data[start_index:end_index]
        for button in self.buttons:
            button.destroy()
        self.buttons = []

        for i, (name, number) in enumerate(page_data):
            row_frame = tk.Frame(self.page_frame)
            row_frame.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

            name_label = tk.Label(row_frame, text=name, font=("Arial", 12))
            name_label.pack(side=tk.LEFT)

            number_label = tk.Label(row_frame, text=number, font=("Arial", 12))
            number_label.pack(side=tk.LEFT, fill=tk.X, expand=True)

            minus_button = tk.Button(row_frame, text="-", command=lambda n=name, l=number_label: self.decrement(n, l))
            minus_button.pack(side=tk.LEFT)

            plus_button = tk.Button(row_frame, text="+", command=lambda n=name, l=number_label: self.increment(n, l))
            plus_button.pack(side=tk.RIGHT)

            self.buttons.append(row_frame)

    def increment(self, name, label):
        current_number = int(label.cget("text"))
        new_number = current_number + 1
        label.config(text=str(new_number))
        # 在增加数字时调用回调函数，传入姓名和新数字
        self.update_data_callback(name, new_number)

    def decrement(self, name, label):
        current_number = int(label.cget("text"))
        if current_number > 0:
            new_number = current_number - 1
            label.config(text=str(new_number))
            self.update_data_callback(name, new_number)

    def next_page(self):
        total_pages = math.ceil(len(self.data) / self.rows_per_page)
        if self.current_page < total_pages:
            self.current_page += 1
            self.update_page()

    def prev_page(self):
        if self.current_page > 1:
            self.current_page -= 1
            self.update_page()


class ImportStudentsDialog:
    def __init__(self, master, import_callback):
        self.master = master
        self.import_callback = import_callback

        self.dialog = tk.Toplevel(self.master)
        self.dialog.title("批量导入学生")
        # 设置对话框的位置
        dialog_width = 550
        dialog_height = 175

        screen_width = self.dialog.winfo_screenwidth()
        screen_height = self.dialog.winfo_screenheight()
        x = (screen_width - dialog_width) // 2
        y = (screen_height - dialog_height) // 2
        self.dialog.geometry(f"{dialog_width}x{dialog_height}+{x}+{y}")

        self.setup_dialog()

    def setup_dialog(self):
        # 添加年级标签和输入框
        grade_label = tk.Label(self.dialog, text="年级：")
        grade_label.grid(row=0, column=0, padx=10, pady=5)
        self.grade_entry = tk.Entry(self.dialog)
        self.grade_entry.grid(row=0, column=1, padx=10, pady=5)

        # 添加班级标签和输入框
        class_label = tk.Label(self.dialog, text="班级：")
        class_label.grid(row=1, column=0, padx=10, pady=5)
        self.class_entry = tk.Entry(self.dialog)
        self.class_entry.grid(row=1, column=1, padx=10, pady=5)

        # 创建右侧标签，用于显示选中的文件全路径
        self.selected_file_label = tk.Label(self.dialog, text="")
        self.selected_file_label.grid(row=2, column=1, padx=10, pady=5)

        file_button = tk.Button(self.dialog, text="点击选择文件", command=self.select_file)
        file_button.grid(row=2, column=0, padx=10, pady=5)

        confirm_button = tk.Button(self.dialog, text="确定", command=self.confirm)
        confirm_button.grid(row=3, column=0, padx=10, pady=5)

        cancel_button = tk.Button(self.dialog, text="取消", command=self.cancel)
        cancel_button.grid(row=3, column=1, padx=10, pady=5)

        self.error_label = tk.Label(self.dialog, text="", fg="red")
        self.error_label.grid(row=4, columnspan=2, padx=10, pady=5)

    def select_file(self):
        file_path = filedialog.askopenfilename(filetypes=[("CSV files", "*.csv")])
        self.dialog.lift()
        if file_path:
            self.selected_file_label.config(text=file_path)

    def confirm(self):
        file_path = self.selected_file_label.cget("text")
        if not file_path:
            self.error_label.config(text="请选择要导入的文件！", fg="red", state="normal")
            return

        grade = self.grade_entry.get()
        class_num = self.class_entry.get()
        if not grade:
            self.error_label.config(text="请输入要导入的年级！", fg="red", state="normal")
            return
        if not class_num:
            self.error_label.config(text="请输入要导入的班级！", fg="red", state="normal")
            return
        self.import_callback(grade, class_num, file_path)
        self.dialog.destroy()

    def cancel(self):
        self.dialog.destroy()


class StudentOperationDialog(tk.Toplevel):
    def __init__(self, master, title, confirm_func):
        super().__init__(master)
        self.title(title)

        # 设置对话框的位置
        dialog_width = 210
        dialog_height = 175
        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()
        x = (screen_width - dialog_width) // 2
        y = (screen_height - dialog_height) // 2
        self.geometry(f"{dialog_width}x{dialog_height}+{x}+{y}")

        # 添加年级标签和输入框
        grade_label = tk.Label(self, text="年级：")
        grade_label.grid(row=0, column=0, padx=10, pady=5)
        self.grade_entry = tk.Entry(self)
        self.grade_entry.grid(row=0, column=1, padx=10, pady=5)

        # 添加班级标签和输入框
        class_label = tk.Label(self, text="班级：")
        class_label.grid(row=1, column=0, padx=10, pady=5)
        self.class_entry = tk.Entry(self)
        self.class_entry.grid(row=1, column=1, padx=10, pady=5)

        # 添加学生标签和输入框
        student_label = tk.Label(self, text="学生：")
        student_label.grid(row=2, column=0, padx=10, pady=5)
        self.student_entry = tk.Entry(self)
        self.student_entry.grid(row=2, column=1, padx=10, pady=5)

        # 添加确定按钮
        confirm_button = tk.Button(self, text="确定",
                                   command=lambda: confirm_func(self.grade_entry, self.class_entry, self.error_label, self, self.student_entry))
        confirm_button.grid(row=3, column=0, padx=10, pady=5)

        # 定义取消按钮的回调函数
        def cancel():
            self.destroy()

        # 添加取消按钮
        cancel_button = tk.Button(self, text="取消", command=cancel)
        cancel_button.grid(row=3, column=1, padx=10, pady=5)

        # 添加错误提示标签
        self.error_label = tk.Label(self, text="", fg="red")
        self.error_label.grid(row=4, columnspan=2, padx=10, pady=5)


class ClassOperationDialog(tk.Toplevel):
    def __init__(self, master, title, confirm_func):
        super().__init__(master)
        self.title(title)

        # 设置对话框的位置
        dialog_width = 210
        dialog_height = 130
        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()
        x = (screen_width - dialog_width) // 2
        y = (screen_height - dialog_height) // 2
        self.geometry(f"{dialog_width}x{dialog_height}+{x}+{y}")

        # 添加年级标签和输入框
        grade_label = tk.Label(self, text="年级：")
        grade_label.grid(row=0, column=0, padx=10, pady=5)
        self.grade_entry = tk.Entry(self)
        self.grade_entry.grid(row=0, column=1, padx=10, pady=5)

        # 添加班级标签和输入框
        class_label = tk.Label(self, text="班级：")
        class_label.grid(row=1, column=0, padx=10, pady=5)
        self.class_entry = tk.Entry(self)
        self.class_entry.grid(row=1, column=1, padx=10, pady=5)

        # 添加确定按钮
        confirm_button = tk.Button(self, text="确定",
                                   command=lambda: confirm_func(self.grade_entry, self.class_entry, self.error_label, self))
        confirm_button.grid(row=2, column=0, padx=10, pady=5)

        # 定义取消按钮的回调函数
        def cancel():
            self.destroy()

        # 添加取消按钮
        cancel_button = tk.Button(self, text="取消", command=cancel)
        cancel_button.grid(row=2, column=1, padx=10, pady=5)

        # 添加错误提示标签
        self.error_label = tk.Label(self, text="", fg="red")
        self.error_label.grid(row=3, columnspan=2, padx=10, pady=5)
